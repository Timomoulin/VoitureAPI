-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 03 mars 2021 à 05:55
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `voiture`
--

-- --------------------------------------------------------

--
-- Structure de la table `activite`
--

DROP TABLE IF EXISTS `activite`;
CREATE TABLE IF NOT EXISTS `activite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `activite`
--

INSERT INTO `activite` (`id`, `label`) VALUES
(1, 'Véhicules sportifs et haut de gamme'),
(2, 'Moto'),
(3, 'Scooter'),
(4, 'Moteur à réaction'),
(5, 'Avion à réaction'),
(6, 'Automobiles');

-- --------------------------------------------------------

--
-- Structure de la table `avoiractivite`
--

DROP TABLE IF EXISTS `avoiractivite`;
CREATE TABLE IF NOT EXISTS `avoiractivite` (
  `idConstructeur` int(11) NOT NULL,
  `idActivite` int(11) NOT NULL,
  PRIMARY KEY (`idConstructeur`,`idActivite`),
  KEY `idActivite` (`idActivite`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `avoiractivite`
--

INSERT INTO `avoiractivite` (`idConstructeur`, `idActivite`) VALUES
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(2, 1),
(2, 6),
(3, 6),
(4, 6);

-- --------------------------------------------------------

--
-- Structure de la table `constructeur`
--

DROP TABLE IF EXISTS `constructeur`;
CREATE TABLE IF NOT EXISTS `constructeur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `creation` int(11) DEFAULT NULL,
  `siege_social` varchar(75) DEFAULT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `constructeur`
--

INSERT INTO `constructeur` (`id`, `nom`, `creation`, `siege_social`, `image`) VALUES
(1, 'Honda', 1948, 'Japon', 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Honda.svg/langfr-200px-Honda.svg.png'),
(2, 'Porsche', 1931, 'Allemagne', 'https://upload.wikimedia.org/wikipedia/fr/thumb/e/e7/Logo_Porsche.svg/langfr-230px-Logo_Porsche.svg.png'),
(3, 'Chevrolet', 1911, 'États-Unis', 'https://upload.wikimedia.org/wikipedia/fr/thumb/4/4d/Chevrolet.svg/langfr-280px-Chevrolet.svg.png'),
(4, 'Renault', 1899, 'France', 'https://upload.wikimedia.org/wikipedia/fr/thumb/4/40/RENAULT_LOGO.svg/langfr-1920px-RENAULT_LOGO.svg.png');

-- --------------------------------------------------------

--
-- Structure de la table `vehicule`
--

DROP TABLE IF EXISTS `vehicule`;
CREATE TABLE IF NOT EXISTS `vehicule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modele` varchar(100) DEFAULT NULL,
  `energie` varchar(100) DEFAULT NULL,
  `annee` int(11) DEFAULT NULL,
  `puissance` int(11) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `constructeur` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_1` (`constructeur`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `vehicule`
--

INSERT INTO `vehicule` (`id`, `modele`, `energie`, `annee`, `puissance`, `image`, `constructeur`) VALUES
(1, 'Porsche Type 64', 'Essence', 1938, 50, 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Rennsport_Reunion_VI_%2845143154931%29.jpg/280px-Rennsport_Reunion_VI_%2845143154931%29.jpg ', 2),
(2, 'Porsche 911', 'Essence', 1974, 180, 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Porsche911sc.jpg/280px-Porsche911sc.jpg', 2),
(3, 'Porsche Carrera GT', 'Essence', 2003, 612, 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Porsche_Carrera_GT_-_Goodwood_Breakfast_Club_%28July_2008%29.jpg/280px-Porsche_Carrera_GT_-_Goodwood_Breakfast_Club_%28July_2008%29.jpg', 2),
(4, 'Honda Odyssey', 'Essence', 1995, 244, 'https://commons.wikimedia.org/wiki/File:2018_Honda_Odyssey_EX-L_3.5L,_front_8.23.19.jpg?uselang=fr', 1),
(5, 'Honda Stream', 'Essence', 2006, 140, 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Hondastream2.jpg/280px-Hondastream2.jpg', 1),
(6, 'Chevrolet Corvette C8 ', 'Essence', 2019, 495, 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Chevrolet_Corvette_C8_Stingray_blue.jpg/280px-Chevrolet_Corvette_C8_Stingray_blue.jpg', 3),
(7, 'Chevrolet Colorado', 'Essence', 2003, 242, 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Chevrolet_Colorado_extcab.jpg/280px-Chevrolet_Colorado_extcab.jpg', 3),
(8, 'Renault Captur', 'Essence', 2013, 120, 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Renault_Captur1.jpg/280px-Renault_Captur1.jpg', 4),
(9, 'Renault Espace IV', 'Essence, Diesel', 2002, 115, 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/2012_Renault_Grand_Espace_2.0.jpg/280px-2012_Renault_Grand_Espace_2.0.jpg', 4);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `avoiractivite`
--
ALTER TABLE `avoiractivite`
  ADD CONSTRAINT `avoiractivite_ibfk_1` FOREIGN KEY (`idConstructeur`) REFERENCES `constructeur` (`id`),
  ADD CONSTRAINT `avoiractivite_ibfk_2` FOREIGN KEY (`idActivite`) REFERENCES `activite` (`id`);

--
-- Contraintes pour la table `vehicule`
--
ALTER TABLE `vehicule`
  ADD CONSTRAINT `vehicule_ibfk_1` FOREIGN KEY (`constructeur`) REFERENCES `constructeur` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
