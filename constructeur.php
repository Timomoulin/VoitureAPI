<?php
// required headers
header("Access-Control-Allow-Origin: *");
//header("Content-Type: application/json; charset=UTF-8");
include 'model/bdd.php';

if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET['id']))
{
    $idConstructeur=htmlspecialchars($_GET['id']);

    $nomTable='constructeur';
    $lesConstructeurs=getAllConstructeurByCol("id",$idConstructeur);
    // var_dump( $lesConstructeurs);
    if(count($lesConstructeurs)==1)
    {
        $lesConstructeurs=$lesConstructeurs[0];
    }
    $jsonData=json_encode($lesConstructeurs);
           echo $jsonData;
}

else if($_SERVER["REQUEST_METHOD"] == "GET")
{
    $nomTable='constructeur';
 $lesConstructeurs=getAllConstructeur();
 $jsonData=json_encode($lesConstructeurs);
        echo $jsonData;
}

else if($_SERVER["REQUEST_METHOD"]=="POST"&& isset($_POST["nom"]) && isset($_POST["image"]))
{
    $unConstructeur= new Constructeur(null,htmlspecialchars($_POST["nom"]),htmlspecialchars($_POST["creation"]),htmlspecialchars($_POST["siegeSocial"]),htmlspecialchars($_POST["image"]));
    // if(isset($_POST["activites"]))
    // {
    //     $idActivites=htmlspecialchars(json_decode($_POST["activites"]));
    //     $lesActivites=array();
    //     foreach ($idActivites as $uneIdActivite)
    //     {
    //         $uneActivite=new Activite ($uneIdActivite,null);
    //         array_push($lesActivites,$uneActivite);
    //     }
    //     $unConstructeur->setActivites($lesActivites);
    // }
    $resultat=addConstructeur($unConstructeur);
    $jsonData=json_encode($resultat);
    echo($jsonData);
}

else if($_SERVER["REQUEST_METHOD"]=="PUT"&& isset($_PUT["id"]))
{
    $unConstructeur= new Constructeur(htmlspecialchars($_PUT["id"]),htmlspecialchars($_PUT["nom"]),htmlspecialchars($_PUT["creation"]),htmlspecialchars($_PUT["siegeSocial"]),htmlspecialchars($_PUT["image"]));
    if(isset($_POST["activites"]))
    {

    }
    updateConstructeur($unConstructeur);
}


?>