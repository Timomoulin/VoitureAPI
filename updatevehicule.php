<?php
// required headers
header("Access-Control-Allow-Origin: *");
// header("Access-Control-Allow-Origin: http://http://localhost:3000");
header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
header("charset=UTF-8");
include 'model/bdd.php';

if($_SERVER["REQUEST_METHOD"]=="POST"&& isset($_POST["id"]))
{
    $id=htmlspecialchars($_POST['id']);
    $modele=htmlspecialchars($_POST['modele']);
    $energie=htmlspecialchars($_POST['energie']);
    $annee=htmlspecialchars($_POST['annee']);
    $puissance=htmlspecialchars($_POST['puissance']);
    $image=htmlspecialchars($_POST['image']);
    $idConstructeur=htmlspecialchars($_POST['constructeur']);
    $vehicule=new Vehicule($id,$modele,$energie,$annee,$puissance,$image);
    $vehicule->constructeur=$idConstructeur;
    // if(isset($_POST["activites"])
    // {

    // }
    updateVehicule($vehicule);
    
}
?>