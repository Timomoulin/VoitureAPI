<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
include 'model/bdd.php';

if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET['id']))
{
    $idVehicule=htmlspecialchars($_GET['id']);
    deleteVehiculeById($idVehicule);
}
?>