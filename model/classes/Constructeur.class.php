<?php
class Constructeur {
    public $id;
    public $nom; 
    public $creation;
    public $siegeSocial;
    public $image;
    public $vehicules= array();
    public $activites= array();

    public function __construct($unId ,$unNom, $creation ,$siegeSocial ,$image ){
        $this->id=$unId;
        $this->nom =$unNom;
        $this->creation =$creation;
        $this->siegeSocial =$siegeSocial;
        $this->image =$image;
    }

    public function getActivites ()
    {
        return $this->activites;
    }

    public function setActivites ($unArray)
    {
        $this->activites=$unArray;
    }
    public function getVehicules ()
    {
        return $this->vehicules;
    }
    public function setVehicules ($unArray)
    {
        $this->vehicules=$unArray;
    }
}
?>