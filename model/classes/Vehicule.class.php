<?php
class Vehicule {
    public $id;
    public $modele; 
    public $energie;
    public $annee;
    public $puissance;
    public $image;
    public $constructeur;

    public function __construct($unId ,$unNom, $energie ,$annee,$puissance ,$image ){
        $this->id=$unId;
        $this->modele =$unNom;
        $this->energie =$energie;
        $this->annee =$annee;
        $this->puissance = $puissance;
        $this->image =$image;
    }
}
?>