<?php
require __DIR__.'/classes/Constructeur.class.php';
require __DIR__.'/classes/Activite.class.php';
require __DIR__.'/classes/Vehicule.class.php';

function seConnecter()
{


$servername = "localhost";
$username = "root";
$password = "";
$myDB="voiture";
    $conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
return $conn;
  }

function getAllConstructeur()
{
try {

    $conn = seConnecter();
    $stmt = $conn->prepare("SELECT * FROM constructeur ");
    $stmt->execute();
  
    $lesResultat = ($stmt->fetchAll());
    $lesConstructeurs= array();
    foreach($lesResultat as $unResultat)
    {
        $unConstructeur=new Constructeur($unResultat[0],$unResultat[1],$unResultat[2],$unResultat[3],$unResultat[4]);
        $stmt2 = $conn->prepare("SELECT * FROM activite inner Join avoiractivite on activite.id=avoiractivite.idActivite where idConstructeur=$unResultat[0]");
        $stmt2->execute();
        $lesResultat2 = ($stmt2->fetchAll());
        $lesActivites=array();
        foreach($lesResultat2 as $unResultat2)
        {
            $uneActivite=new Activite ($unResultat2[0],$unResultat2[1]);
            array_push($lesActivites,$uneActivite);
        }
        $unConstructeur->setActivites($lesActivites);
        $lesVehicules=getAllVehiculeByCol("constructeur",$unResultat[0]);
        $unConstructeur->setVehicules($lesVehicules);
        array_push($lesConstructeurs,$unConstructeur);
    }
    return $lesConstructeurs;
  } catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  $conn = null;
}

function getAllConstructeurByCol($nomCol,$valeurCol)
{
try {
  $conn = seConnecter();
    $stmt = $conn->prepare("SELECT * FROM constructeur where $nomCol = :valeurCol ");
    
    $stmt ->bindParam(':valeurCol',$valeurCol);
    $stmt->execute();
  
    $lesResultat = ($stmt->fetchAll());
    $lesConstructeurs= array();
    foreach($lesResultat as $unResultat)
    {
        $unConstructeur=new Constructeur($unResultat[0],$unResultat[1],$unResultat[2],$unResultat[3],$unResultat[4]);
        $stmt2 = $conn->prepare("SELECT * FROM activite inner Join avoiractivite on activite.id=avoiractivite.idActivite where idConstructeur=$unResultat[0]");
        $stmt2->execute();
        $lesResultat2 = ($stmt2->fetchAll());
        $lesActivites=array();
        foreach($lesResultat2 as $unResultat2)
        {
            $uneActivite=new Activite ($unResultat2[0],$unResultat2[1]);
            array_push($lesActivites,$uneActivite);
        }
        $unConstructeur->setActivites($lesActivites);
        $lesVehicules=getAllVehiculeByCol("constructeur",$unResultat[0]);
        $unConstructeur->setVehicules($lesVehicules);
        array_push($lesConstructeurs,$unConstructeur);
    }
    return $lesConstructeurs;
  } catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  $conn = null;
}

function getAllVehiculeByCol($nomCol,$valeurCol)
{
  try {
    $conn = seConnecter();
        $stmt = $conn->prepare("SELECT * FROM vehicule where $nomCol = :valeurCol ");
         //$stmt ->bindParam(':nomCol',$nomCol);
        $stmt ->bindParam(':valeurCol',$valeurCol);
        $stmt->execute();
      
        $lesResultat = ($stmt->fetchAll());
        $lesVehicules= array();
        foreach($lesResultat as $unResultat)
        {
            $unVehicule=new Vehicule($unResultat[0],$unResultat[1],$unResultat[2],$unResultat[3],$unResultat[4],$unResultat[5]);
            array_push($lesVehicules,$unVehicule);
        }
        return $lesVehicules;
      } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
      }
      $conn = null;
}

function getAllActivite()
{
  try {
    $conn = seConnecter();
        $stmt = $conn->prepare("SELECT * FROM activite");
        $stmt->execute();
      
        $lesResultat = ($stmt->fetchAll());
        $lesActivites= array();
        foreach($lesResultat as $unResultat)
        {
            $uneActivite=new Activite($unResultat[0],$unResultat[1]);
            array_push($lesActivites,$uneActivite);
        }
        return $lesActivites;
      } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
      }
      $conn = null;
}

function getAllActiviteBy($nomCol,$valeurCol)
{
  try {
    $conn = seConnecter();
        $stmt = $conn->prepare("SELECT * FROM activite inner join avoiractivite on activite.id=avoiractivite.idActivite where $nomCol = :valeurCol");
        //$stmt ->bindParam(':nomCol',$nomCol);
        $stmt ->bindParam(':valeurCol',$valeurCol);
        $stmt->execute();
      
        $lesResultat = ($stmt->fetchAll());
        $lesActivites= array();
        foreach($lesResultat as $unResultat)
        {
            $uneActivite=new Activite($unResultat[0],$unResultat[1]);
            array_push($lesActivites,$uneActivite);
        }
        return $lesActivites;
      } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
      }
      $conn = null;
}

function addConstructeur($unConstructeur)
{
  try {
    $conn = seConnecter();
        $stmt = $conn->prepare("INSERT INTO constructeur values (null,'$unConstructeur->nom',$unConstructeur->creation,'$unConstructeur->siegeSocial','$unConstructeur->image') ");
        $stmt->execute();
        $idConctructeur = $conn->lastInsertId();
        if($unConstructeur->getActivites()!==null)
        {
        foreach($unConstructeur->getActivites() as $uneActivite)
        {
        $stmt2 = $conn->prepare("INSERT INTO avoiractivite values ($idConctructeur,$uneActivite->id)");
        $stmt2->execute();
        }
      }
        return getAllConstructeurByCol("id",$idConctructeur);
      } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
      }
      $conn = null;
}
function addVehicule($unVehicule)
{
  try {
    $conn = seConnecter();
        $stmt = $conn->prepare("INSERT INTO vehicule values (null,'$unVehicule->modele','$unVehicule->energie','$unVehicule->annee','$unVehicule->puissance','$unVehicule->image','$unVehicule->constructeur') ");
        $stmt->execute();
        $idVehicule = $conn->lastInsertId();
    
        return getAllVehiculeByCol("id",$idVehicule);
      } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
      }
      $conn = null;
}

function deleteConstructeurById($uneID)
{
  try {
  $conn = seConnecter();
  $stmt = $conn->prepare("DELETE FROM avoiractivite where idConstructeur=:uneID");
  $stmt ->bindParam(':uneID',$uneID);
  $stmt->execute();
  $stmt3 = $conn->prepare("DELETE FROM vehicule where constructeur=:uneID");
  $stmt3 ->bindParam(':uneID',$uneID);
  $stmt3->execute();
  $stmt2 = $conn->prepare("DELETE FROM constructeur where id=:uneID");
  $stmt2 ->bindParam(':uneID',$uneID);
  $stmt2->execute();

  }
  catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
}

function deleteVehiculeById($uneID)
{
  try {
    $conn = seConnecter();
    $stmt = $conn->prepare("DELETE FROM vehicule where id=:uneID");
    $stmt ->bindParam(':uneID',$uneID);
    $stmt->execute();}
    catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
  }
}

function updateConstructeur($unConstructeur)
{
  try{
    $conn = seConnecter();
    $stmt = $conn->prepare("UPDATE constructeur SET nom='$unConstructeur->nom',creation=$unConstructeur->creation,siege_social='$unConstructeur->siegeSocial',image='$unConstructeur->image' where id=$unConstructeur->id ");
    $stmt->execute();
    if($unConstructeur->getActivites()!==null)
    {
      $stmt2 = $conn->prepare("DELETE FROM avoiractivite where idConstructeur=:uneID");
      $stmt2 ->bindParam(':uneID',$unConstructeur->id);
      $stmt2->execute();
    foreach($unConstructeur->getActivites() as $uneActivite)
    {
     
    $stmt3 = $conn->prepare("INSERT INTO avoiractivite values ($idConctructeur,$uneActivite->id)");
    $stmt3->execute();
    }
  }
  }
  catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  $conn = null;
}
function updateVehicule($unVehicule)
{
  try {
    $conn = seConnecter();
        $stmt = $conn->prepare("UPDATE vehicule SET modele='$unVehicule->modele',energie='$unVehicule->energie',annee=$unVehicule->annee,puissance=$unVehicule->puissance,image='$unVehicule->image',constructeur=$unVehicule->constructeur where vehicule.id=:id");
        $stmt ->bindParam(':id',$unVehicule->id);
        $stmt->execute();
    
        return getAllVehiculeByCol("id",$unVehicule->id);
      } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
        var_dump($stmt);
      }
      $conn = null;
}
?>