<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
include 'model/bdd.php';

if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET['idConstructeur']))
{
    $idConstructeur=htmlspecialchars($_GET['idConstructeur']);

    $lesVehicules=getAllVehiculeByCol("constructeur",$idConstructeur);
    $jsonData=json_encode($lesVehicules);
           echo $jsonData;
}

else if($_SERVER["REQUEST_METHOD"] == "GET"&& isset($_GET['id']))
{
    $id=htmlspecialchars($_GET['id']);
    $lesVehicules=getAllVehiculeByCol("id",$id);
    if(count($lesVehicules)==1)
    {
        $lesVehicules=$lesVehicules[0];
    }
    $jsonData=json_encode($lesVehicules);
           echo $jsonData;
}
else if($_SERVER["REQUEST_METHOD"] == "POST")
{
    $modele=htmlspecialchars($_POST['modele']);
    $energie=htmlspecialchars($_POST['energie']);
    $annee=htmlspecialchars($_POST['annee']);
    $puissance=htmlspecialchars($_POST['puissance']);
    $image=htmlspecialchars($_POST['image']);
    $idConstructeur=htmlspecialchars($_POST['constructeur']);
    $vehicule=new Vehicule(null,$modele,$energie,$annee,$puissance,$image);
    $vehicule->constructeur=$idConstructeur;
    $resultat=json_encode(addVehicule($vehicule));
    echo $jsonData;
}

?>