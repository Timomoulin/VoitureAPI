<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>API Voiture </title>
</head>
<body>
    <a href="./constructeur.php" target="_blank">Les constructeurs</a> <br/>
    <a href="./constructeur.php?id=1" target="_blank">Honda</a> <br/>
    <br/>
    <a href="./vehicule.php?idConstructeur=1" target="_blank">Les véhicules Honda</a> <br/>
    <a href="./vehicule.php?id=1" target="_blank">Un véhicule</a> <br/>

    <h1>Test Ajout de constructeur</h1>
    <form method="POST" action="./createconstructeur.php">
    <div>method="POST" action="./constructeur.php"</div>
        <label for="">Nom </label>
        <input name="nom" type="text">
        <label for="">Année de création </label>
        <input name="creation" type="number" min="1800" >
        <label for="">Pays</label>
        <input name="siegeSocial" type="text">
        <label for="">URL image</label>
        <input type="text" name="image">
        <button>Test</button>
    </form>
    <h1>Test Modification de constructeur</h1>
    <form method="POST" action="./updateconstructeur.php">
    <div>method="POST" action="./updateconstructeur.php"</div>
    <label for="">Nom </label>
        <input name="id" value="1">
        <label for="">Nom </label>
        <input name="nom" type="text" value="honda">
        <label for="">Année de création </label>
        <input name="creation" type="number" min="1800" value="1948" >
        <label for="">Pays</label>
        <input name="siegeSocial" type="text" value="Japon">
        <label for="">URL image</label>
        <input type="text" name="image" value="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Honda.svg/langfr-200px-Honda.svg.png">
        <button>Test</button>
    </form>
    <h1>Delete </h1>
    <a href="./deleteconstructeur.php?id=1">Honda</a>

    <h1>Ajout de vehicule</h1>
    <form action="./vehicule.php" method="post">
    <div> action=http://localhost:80/vehicule.php method=post</div>
    Modele :<input type="text" name="modele">
    Energie : <input type="text" name="energie">
    Annee : <input type="number" name="annee">
    Puissance : <input type="number" name="puissance">
    Image : <input type="text" name="image">
    idConstructeur : <input type="number"name="constructeur">
    <button>Test</button>
    </form>

    <h1>Modification de vehicule</h1>
    <form action="./updatevehicule.php" method="post">
    <div> action=http://localhost:80/updatevehicule.php method=post</div>
    Id : <input type="number" name="id"> 
    Modele :<input type="text" name="modele">
    Energie : <input type="text" name="energie">
    Annee : <input type="number" name="annee">
    Puissance : <input type="number" name="puissance">
    Image : <input type="text" name="image">
    idConstructeur : <input type="number"name="constructeur">
    <button>Test</button>
    </form>
</body>
</html>