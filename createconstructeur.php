<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
include 'model/bdd.php';


if($_SERVER["REQUEST_METHOD"]=="POST"&& isset($_POST["nom"]) && isset($_POST["image"]))
{
    $unConstructeur= new Constructeur(null,htmlspecialchars($_POST["nom"]),htmlspecialchars($_POST["creation"]),htmlspecialchars($_POST["siegeSocial"]),htmlspecialchars($_POST["image"]));
    if(isset($_POST["activites"]))
    {
        $idActivites=htmlspecialchars(json_decode($_POST["activites"]));
        $lesActivites=array();
        foreach ($idActivites as $uneIdActivite)
        {
            $uneActivite=new Activite ($uneIdActivite,null);
            array_push($lesActivites,$uneActivite);
        }
        $unConstructeur->setActivites($lesActivites);
    }
    
    $resultat=addConstructeur($unConstructeur);
    $jsonData=json_encode($resultat);
    echo($jsonData);
}

?>